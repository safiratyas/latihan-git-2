// Require function will allow you to call the module
const readline = require('readline');

// Initiate the readline to use stdin and stdout as input and output
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Start up message
console.clear(); // To clear up the terminal
console.log("I can calculate the rectangle area for you");

// The function
function calcRectangleArea(width, length) {
  return width * length;
}

// Ask the width
rl.question('Width: ', (width) => {
  
  // Ask the length
  rl.question('Length: ', (length) => {
    console.log(
      "Here's the result:",
      calcRectangleArea(width, length)
    )
    
    rl.close();
  })
});

module.exports = {
    rectangle
  }